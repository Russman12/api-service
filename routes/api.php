<?php

use Illuminate\Http\Request;
use App\Vehicle;
use App\Manufacturer;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');


Route::group(['middleware' => 'auth:api'], function() {
    Route::bind('manufacturer', function($id) {
        return Manufacturer::with('vehicles')->findOrFail($id);
    });
    Route::get('manufacturers', 'ManufacturerController@index');
    Route::get('manufacturers/{manufacturer}', 'ManufacturerController@show');
    Route::post('manufacturers', 'ManufacturerController@store');
    Route::put('manufacturers/{manufacturer}', 'ManufacturerController@update');
    Route::delete('manufacturers/{manufacturer}', 'ManufacturerController@destroy');

    Route::bind('vehicle', function($id) {
        return Vehicle::with('manufacturer')->findOrFail($id);
    });
    Route::get('vehicles', 'VehicleController@index');
    Route::get('vehicles/{vehicle}', 'VehicleController@show');
    Route::post('vehicles', 'VehicleController@store');
    Route::put('vehicles/{vehicle}', 'VehicleController@update');
    Route::delete('vehicles/{vehicle}', 'VehicleController@destroy');
});
