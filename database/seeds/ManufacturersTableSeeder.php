<?php

use Illuminate\Database\Seeder;
use App\Manufacturer;

class ManufacturersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Manufacturer::truncate();

        $manufacturerNames = ['Chevrolet', 'Ford', 'Toyota', 'Honda'];

        foreach ($manufacturerNames as $name) {
            Manufacturer::create([
                'name' => $name
            ]);
        }

    }
}
