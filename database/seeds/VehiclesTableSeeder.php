<?php

use Illuminate\Database\Seeder;
use App\Vehicle;

class VehiclesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vehicle::truncate();

        $faker = \Faker\Factory::create();

        $manufacturer1Models = array ('Camaro', 'Cruze', 'Corvette', 'Spark', 'Impala', 'Tahoe', 'Malibou', 'Traverse');
        $manufacturer2Models = array ('Expidition', 'Focus', 'Mustang', 'Fiesta', 'Escape', 'Edge', 'Fusion', 'Explorer', 'Taurus', 'Ranger', 'Transit');
        $manufacturer3Models = array ('Yaris','Corolla','Camry', 'Avalon', 'Sienna');
        $manufacturer4Models = array ('Accord','Amaze','Avancier', 'Ballade', 'Brio', 'City', 'Civic', 'BR-V');

        $numVehicles = 10;
        for($i = 0; $i < $numVehicles; $i++) {
            $manufacturerID = $faker->numberBetween(1, 4);

            if($manufacturerID === 1) {
                $model = $faker->randomElement($manufacturer1Models);
            } else if($manufacturerID === 2) {
                $model = $faker->randomElement($manufacturer2Models);
            } else if($manufacturerID === 3) {
                $model = $faker->randomElement($manufacturer3Models);
            } else if($manufacturerID === 4) {
                $model = $faker->randomElement($manufacturer4Models);
            }

            Vehicle::create([
                'vin' => $faker->unique()->regexify('[A-Z0-9]{17}'),
                'model' => $model,
                'color' => $faker->safeColorName,
                'year' => $faker->year($max = 'now'),
                'manufacturer_id' => $manufacturerID,
            ]);
        }
    }
}
