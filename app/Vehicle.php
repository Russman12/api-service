<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = ['vin', 'model', 'color', 'year', 'manufacturer_id'];
    //protected $with = ['manufacturer'];

    /**
     * Get the manufacturer of the vehicle
     */
    public function manufacturer()
    {
        return $this->belongsTo('App\Manufacturer');
    }
}
