<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    protected $fillable = ['name'];


    /**
     * Get the vehicles related to this manufacturer
     */
    public function vehicles()
    {
        return $this->hasMany('App\Vehicle');
    }
}
