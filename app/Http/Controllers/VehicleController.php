<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehicle;
use App\Manufacturer;
use Validator;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Vehicle::with('manufacturer')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vin' => 'required|unique:vehicles|size:17',
            'model' => 'required|max:255',
            'color' => 'required|max:255',
            'year' => 'required|digits:4|integer',
            'manufacturer_id' => 'required|integer|exists:manufacturers,id',
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $vehicle = Vehicle::create($request->all());

            return response()->json($vehicle, 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        return $vehicle;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        $validator = Validator::make($request->all(), [
            'model' => 'filled|max:255',
            'color' => 'filled|max:255',
            'year' => 'filled|digits:4|integer',
            'manufacturer_id' => 'filled|integer|exists:manufacturers,id',
        ]);
        

        if($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $vehicle->update($request->all());

            return response()->json($vehicle, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle $vehicle)
    {
        $vehicle->delete();

        return response()->json(null, 204);
    }
}
